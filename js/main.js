var myEmail = 'w3tra@mail.ru';
var baseURL = 'http://146.185.154.90:8000/blog/' + myEmail;
var profileInfo = {};
var editProfileButton = $('#save-profile-info');
var editFirstNameInput = $('#edit-firstname');
var editLastNameInput = $('#edit-lastname');
var firstNameSpan = $('#first-name');
var lastNameSpan = $('#last-name');
var followUserButton = $('#follow-user');
var followUserInput = $('#follow-user-email');
var feedBlock = $('#feed');
var lastMessageDateTime = null;
var sendMessageButton = $('#send-message-button');
var sendMessageInput = $('#send-message-input');

var getProfile = function () {
    return $.ajax(baseURL + '/profile');
};

var editProfile = function (firstName, lastName) {
    return $.ajax({
        method: 'POST',
        url: baseURL + '/profile',
        data: {firstName: firstName, lastName: lastName}
    })
};

var getFeed = function () {
    return $.ajax({
        url: baseURL + '/posts',
        data: jQuery.param({datetime: lastMessageDateTime})
    });
};

var updateLastMessageDateTime = function (messages) {
    if (messages.length) {
        lastMessageDateTime = messages.slice(-1)[0].datetime;
        return messages
    }
};

var sendPost = function (message) {
    return $.ajax({
        method: 'POST',
        url: baseURL + '/posts',
        data: {message: message}
    })
};

var subscribe = function (email) {
    return $.ajax({
        method: 'POST',
        url: baseURL + '/subscribe',
        data: {email: email}
    })
};

var updateProfileInfo = function (profile) {
    profileInfo = profile;
    editFirstNameInput.val(profile.firstName);
    editLastNameInput.val(profile.lastName);
    return profileInfo;
};

var renderProfileName = function (profile) {
    firstNameSpan.html(profile.firstName);
    lastNameSpan.html(profile.lastName);
};

var renderFeed = function (messages) {
    if (messages) {
        messages.forEach(function (message) {
            renderMessage(message)
        })
    }
};

var renderMessage = function (message) {
    var messageDiv = $('<div class="feed-message">');
    var messageDateTime = $(`<p class="datetime">${moment(message.datetime).format('MMMM Do YYYY, h:mm:ss a')}</p>`);
    messageDiv.append(messageDateTime);
    var messageUserSpan = $(`<span class="feed-username"><b>${message.user.firstName} ${message.user.lastName}</b></span>`);
    messageUserSpan.append($('<span> said:</span>'));
    var messageText = $(`<p class="feed-message-text">${message.message}</p>`);
    messageDiv.append(messageUserSpan).append(messageText);
    feedBlock.prepend(messageDiv);
};

var editProfileAndRenderNewInfo = function () {
    var firstName = editFirstNameInput.val();
    var lastName = editLastNameInput.val();
    if (firstName.length > 0 && lastName.length > 0) {
        editProfile(firstName, lastName)
            .then(updateProfileInfo)
            .then(renderProfileName)
            .then(function () {
                $('#editProfile').modal('toggle');
            })
    } else {
        $('#editProfile').modal('toggle');
    }
};

var subscribeToUserAndToggleModal = function () {
    var email = followUserInput.val();
    subscribe(email).then(function () {
        $('#followUser').modal('toggle');
    })
};

var sendMessageAndClearInput = function () {
    var message = sendMessageInput.val();
    if (message.length > 0) {
        sendPost(message);
        sendMessageInput.val('');
    } else {
        alert('Enter some message');
    }
};

editProfileButton.on('click', editProfileAndRenderNewInfo);
followUserButton.on('click', subscribeToUserAndToggleModal);
sendMessageButton.on('click', sendMessageAndClearInput);

getProfile()
    .then(updateProfileInfo)
    .then(renderProfileName)
    .then(getFeed)
    .then(updateLastMessageDateTime)
    .then(renderFeed);

setInterval(function () {
    getFeed()
        .then(updateLastMessageDateTime)
        .then(renderFeed);
}, 2000);



